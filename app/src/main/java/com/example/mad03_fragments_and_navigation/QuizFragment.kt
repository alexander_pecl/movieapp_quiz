package com.example.mad03_fragments_and_navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.mad03_fragments_and_navigation.databinding.FragmentQuizBinding
import com.example.mad03_fragments_and_navigation.models.QuestionCatalogue
import com.example.mad03_fragments_and_navigation.utils.QuizViewModel


class QuizFragment : Fragment() {
    private lateinit var viewModel: QuizViewModel
    private lateinit var binding: FragmentQuizBinding
    private val questions = QuestionCatalogue().defaultQuestions    // get a list of questions for the game

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        viewModel = ViewModelProvider(this).get(QuizViewModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_quiz, container, false)
        binding.index = viewModel.index.value?.plus(1)
        binding.questionsCount = questions.size
        binding.question = questions[viewModel.index.value!!]

        viewModel.index.observe(viewLifecycleOwner, Observer { newIndex ->
            binding.index = newIndex + 1
            if (newIndex < questions.size) {
                binding.question = questions[newIndex]
            } else {
                view?.findNavController()?.navigate(QuizFragmentDirections.actionQuizFragmentToQuizEndFragment(viewModel.score.value!!, questions.size))
            }
        })

        binding.btnNext.setOnClickListener {
            nextQuestion()
        }

        return binding.root
    }

    private fun nextQuestion() {
        // get selected answer
        // check if is correct answer
        // update score
        // check if there are any questions left
        // show next question OR
        // navigate to QuizEndFragment
        if (binding.answerBox.checkedRadioButtonId == -1) {
            Toast.makeText(requireContext(), "Please enter an Answer.", Toast.LENGTH_SHORT).show()
            return
        }

        viewModel.answers = binding.answerBox
        viewModel.selectedAnswer = binding.answerBox.findViewById(binding.answerBox.checkedRadioButtonId)

        viewModel.checkQuestion()
        binding.answerBox.clearCheck()
    }
}