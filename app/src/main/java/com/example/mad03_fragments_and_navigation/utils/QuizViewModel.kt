package com.example.mad03_fragments_and_navigation.utils

import android.annotation.SuppressLint
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mad03_fragments_and_navigation.models.Answer
import com.example.mad03_fragments_and_navigation.models.Question
import com.example.mad03_fragments_and_navigation.models.QuestionCatalogue

class QuizViewModel : ViewModel() {
    var score = MutableLiveData<Int>()
    var index = MutableLiveData<Int>()

    var questions = QuestionCatalogue().defaultQuestions
    @SuppressLint("StaticFieldLeak")
    lateinit var answers : RadioGroup
    @SuppressLint("StaticFieldLeak")
    lateinit var selectedAnswer : RadioButton

    init {
        score.value = 0
        index.value = 0
    }

    private fun nextQuestion() {
        index.value = (index.value)?.plus(1)
    }

    private fun onCorrect() {
        score.value = (score.value)?.plus(1)
    }

    fun checkQuestion() {
        if (questions[index.value!!].answers[answers.indexOfChild(selectedAnswer)].isCorrectAnswer) {
            onCorrect()
        }
        nextQuestion()
    }
}